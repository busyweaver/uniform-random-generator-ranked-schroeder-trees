module Sampler where

import Math.Combinat
import Data.List
import Data.Function (fix)


fromPartition' :: Partition -> [Integer]
fromPartition' (Partition_ part) = map fromIntegral part

partToV :: [Integer] -> [Integer]

partToV p = scanl (-) (sum p) p --consecutive differences

vToArities :: [Integer] -> [Integer]

vToArities (a:b:c) = [a-b] ++ (reverse . sort  $ tailPart ++ lastElement)
    where 
        p = b:c --ignore the first element
        triplets = zip3 p (tail p) (tail $ tail p) -- construct a list of triplets (p_i, p_i+1, p_i+2) out of the original p
        howMany = map (\(a,b,c) -> fromIntegral $ a - 2*b + c) triplets -- apply the map p_i - 2*p_{i+1} + p_{i+2}
        tailPart = foldl (++) [] $ map (\(index, element) -> replicate (fromIntegral element) index ) (zip [1..] howMany) --repeat index according to howMany
        secondToLastElementWithIndex = drop 1 . take 2 . reverse $ zip [1..] p --extract the second to last element of p with its index p_{n-1}
        replicateStuff =  map (\(index, element) -> replicate (fromIntegral element) index) secondToLastElementWithIndex -- repeat its index p_{n-1} times
        lastElement = replicateStuff !! 0 -- extraction


listsAV :: Integer -> [([Integer], [Integer])]

listsAV n = map makeTuples (filter (\x -> not $ x == [fromIntegral n]) (map fromPartition' (partitions $ fromIntegral n))) -- filter out trivial partition
    where 
        makeTuples = (\x -> (partToV x, vToArities $ partToV x))

--yo = map makeTriplets (filter (\x -> not $ x == [7]) (map fromPartition (partitions 7)))
--    where 
--       makeTriplets = (\x -> (x,partToV x, vToArities $ partToV x))

count :: Eq a => a -> [a] -> Int
count x = length . filter (x == )

occ :: [Integer] -> [Integer]
occ (x:xs) = [fromIntegral $ count i xs | i <- [0 .. maximum xs]]

phi :: Integer -> Integer
phi 2 = 1
phi _ = 0

coeff :: [Integer] -> Integer -> (Integer -> Integer) -> Integer

coeff l n phi = round $ product $ [toRational $ (/) (toRational $ factorial (n-k)) (toRational $ factorial (n - k - (fromIntegral $ length l) + 1))] ++ bigP
    where
        k = n - (l !! 0)
        e = tail (occ l)
        bigP = [toRational $ (/) (toRational $ (phi (i + 1)) ^ ei) (toRational $ factorial ei) | (i,ei) <- zip [1..] e]

mainCo :: [([Integer], [Integer])] -> Integer -> (Integer -> Integer) -> [Integer]

mainCo l n phi = map sumOrZero [[b | (a,b) <- partialThings, a == i] | i <- [0..n-1]]
    where
        partialThings = map (\(x,y) -> (x !! 1, coeff y n phi)) l
        sumOrZero [] = 0
        sumOrZero x = sum x

computeB ::  (Int -> Integer) -> Int -> Integer

computeB mComp 0  = 0
computeB mComp 1  = 1

computeB mComp n = sum [ ((guys $ fromIntegral n) !! (fromIntegral $ i))  * (mComp (n-i)) | i <- [1..n-1]]
    where
        guys n = mainCo (listsAV n) n phi

memoize :: (Int -> a) -> (Int -> a)
memoize f = (map f [0 ..] !!)

memoizedComputeB :: Int -> Integer
memoizedComputeB = fix (memoize . computeB)