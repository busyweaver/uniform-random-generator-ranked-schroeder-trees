module Sampler where

import Math.Combinat hiding (Node, Leaf)
import Data.List
import Data.Function (fix)
import System.Random
import Control.Monad.State

--phi gives the weighted degree function
phi :: Integer -> Integer
phi 0 = 0
phi 1 = 0
phi n = 1

-- allowedReps gives the subset of allowed repetitions
-- in fact we should check n-1
allowedReps :: Integer -> Bool
allowedReps 1 = True 
allowedReps n = False

--converts partitions to lists of integers
fromPartition' :: Partition -> [Integer]
fromPartition' (Partition_ part) = map fromIntegral part

--corresponds to the mapping \mathcal{F} in chapter 7
partToV :: [Integer] -> [Integer]

partToV p = scanl (-) (sum p) p --consecutive differences

--corresponds to the mapping \mathcal{G} in chapter 7
vToArities :: [Integer] -> [Integer]

vToArities (a:b:c) = [a-b] ++ (reverse . sort  $ tailPart ++ lastElement)
    where 
        p = b:c --ignore the first element
        triplets = zip3 p (tail p) (tail $ tail p) -- construct a list of triplets (p_i, p_i+1, p_i+2) out of the original p
        howMany = map (\(a,b,c) -> fromIntegral $ a - 2*b + c) triplets -- apply the map p_i - 2*p_{i+1} + p_{i+2}
        tailPart = foldl (++) [] $ map (\(index, element) -> replicate (fromIntegral element) index ) (zip [1..] howMany) --repeat index according to howMany
        secondToLastElementWithIndex = drop 1 . take 2 . reverse $ zip [1..] p --extract the second to last element of p with its index p_{n-1}
        replicateStuff =  map (\(index, element) -> replicate (fromIntegral element) index) secondToLastElementWithIndex -- repeat its index p_{n-1} times
        lastElement = replicateStuff !! 0 -- extraction

--computes the set A_{n,\phi,r}
listsAV :: Integer -> [([Integer], [Integer])]

listsAV n = filterListsAV $ map makeTuples (filter (\x -> not $ x == [fromIntegral n]) (map fromPartition' (partitions $ fromIntegral n))) -- filter out trivial partition
    where 
        makeTuples = (\x -> (partToV x, vToArities $ partToV x))

filterListsAV ::  [([Integer], [Integer])] ->  [([Integer], [Integer])]

filterListsAV listsAV = filter (\(v,a) -> allowedReps $ (fromIntegral $ length a) - 1) listsAV

count :: Eq a => a -> [a] -> Int
count x = length . filter (x == )

--occ function as given in the text
occ :: [Integer] -> [Integer]
occ (x:xs) = [fromIntegral $ count i xs | i <- [0 .. maximum xs]]

modifyA :: [([Integer], [Integer])] -> [([Integer], [Integer])]

modifyA avList = avList ++ (concat $ map modifier avList)

modifier :: ([Integer], [Integer]) -> [([Integer], [Integer])]

modifier (listV,listA) = [(listV, newA) | newA <- newALists]
    where 
        len = length listA
        addedZeroes = [(fromIntegral $ i) | i <- [len - 1 .. (fromIntegral $ (listA !! 0) - 1)]]
        filteredZeroes = [allowedReps ((fromIntegral $ len - 1) + (fromIntegral $ i)) | i <- addedZeroes]

        indexedFilteredZeroes = zip filteredZeroes [1 ..]

        filteredIndexed = filter fst indexedFilteredZeroes

        secondElements = map snd filteredIndexed

        newALists = [listA ++ replicate n 0 | n <- secondElements]
 
--product appearing inside the recurrence
coeff :: [Integer] -> Integer -> (Integer -> Integer) -> Integer

coeff l n phi = round $ product $ [toRational $ (/) (toRational $ factorial (n-k)) (toRational $ factorial (n - k - (fromIntegral $ length l) + 1))] ++ bigP
    where
        k = n - (l !! 0)
        e = (occ l)
        bigP = [toRational $ (/) (toRational $ (phi (i + 1)) ^ ei) (toRational $ factorial ei) | (i,ei) <- zip [0 ..] e]

--coefficient in front of each B 
mainCo :: [([Integer], [Integer])] -> Integer -> (Integer -> Integer) -> [Integer]

mainCo l n phi = map sumOrZero [[b | (a,b) <- partialThings, a == i] | i <- [0..n-1]]
    where
        partialThings = map (\(x,y) -> (x !! 1, coeff y n phi)) l
        sumOrZero [] = 0
        sumOrZero x = sum x

--computing B with phi
--hardcoding phi for now, since its much faster when you just memoize a function of an integer instead of one that also takes phi
computeB ::  (Int -> (Integer, [Integer])) -> Int -> (Integer, [Integer])

computeB mComp 0  = (0, [0])
computeB mComp 1  = (1, [0])

computeB mComp n = (sum [ ( guys !! (fromIntegral $ i))  * (fst (mComp (n-i))) | i <- [1..n-1]], guys)
    where
        guys = mainCo (modifyA $ listsAV (fromIntegral $ n)) (fromIntegral $ n) phi

memoize :: (Int -> a) -> (Int -> a)
memoize f = (map f [0 ..] !!)

memoizedComputeB :: Int -> (Integer, [Integer])
memoizedComputeB = fix (memoize . computeB)

data SubStep = SubStep {newArity :: Integer, color :: Integer, whichLeaf :: Integer} deriving (Show)

sampleTree :: StdGen -> Int -> [[SubStep]]

-- sampling a tree of size n in the class of trees parametrized by phi and allowedReps
sampleTree gen n = 
    if n > 1 then sampleTree newGen (n-k) ++ [step]
    else []
    where
        bUpToN = [memoizedComputeB i | i <- [0..n]]
        (bn, treeCo) = head . reverse $ bUpToN
        (treeNum, newGen) = randomR (1,bn) gen
        bs = map fst bUpToN
        zipped = zip treeCo (reverse bs)
        products = map (\(a,b) -> a * b) zipped
        scannedDiffs = scanl (-) treeNum products
        --drop one since for some reason the first guy is the default value
        (kMinusOne, _) =  head . reverse $ filter (\(a,b) -> b > 0) (zip [0..] (drop 1 scannedDiffs)) --get the last positive value (this works since scannedDiffs is monotone decreasing)
        k = kMinusOne + 1

        aritiesFromList = map snd (modifyA $ listsAV (fromIntegral $ n))
        arities = filter (\x -> (fromIntegral $ head x) == (n-k)) aritiesFromList
        weights = map (\x -> coeff x (fromIntegral $ n) phi) arities

        weightedArityIndices = concat $ map (\(a,b) -> replicate (fromIntegral $ a) b) $ zip weights [0..]
        randomWeightIndex = weightedArityIndices !! fst (randomR (0, length weightedArityIndices - 1) gen :: (Int, StdGen)) -- choose a random arity (by choosing its index)
        
        randomArities = drop 1 $ arities !! randomWeightIndex
        randomColors = map (\x -> fst $ randomR (1, phi (x+1)) gen) $ randomArities
        randomLeafs = take (length arities) $ Data.List.nub $ randomRs (1,n-k) gen
        finalStep = zip3 randomArities randomColors randomLeafs
        step = map (\(a,b,c) -> SubStep (a+1) b (fromIntegral $ c)) finalStep

-- tree is either a leaf or a node
data IncTree =  Leaf Int -- either a leaf which holds its (clockwise-visitation) index
                | Node Int Int [IncTree] -- or a node with label, color, children
                deriving (Show)

updateNodeIndices :: IncTree -> Int -> Int -> IncTree

updateNodeIndices (Leaf lIndex) idxOfChange incrAmount = Leaf newIndex
    where
        newIndex = 
            if lIndex > idxOfChange then lIndex + incrAmount - 1
            else lIndex

updateNodeIndices (Node idx clr children) idxOfChange incrAmount = Node idx clr $ map (\x -> updateNodeIndices x idxOfChange incrAmount) children

insertNode :: IncTree -> Int -> SubStep -> IncTree

insertNode (Leaf idx) currentStep (SubStep arity color whichLeaf) = 
    if idx == wwhichLeaf then (Node currentStep (fromIntegral $ color) (replicate aarity $ Leaf 0))
    else (Leaf idx)
    where
        wwhichLeaf = fromIntegral $ whichLeaf
        aarity = fromIntegral $ arity

insertNode (Node idx clr children) currentStep substitutionStep =
    Node idx clr $ map (\x -> insertNode x currentStep substitutionStep) children

applySubStep :: IncTree -> Int -> SubStep -> IncTree

applySubStep currentTree currentStep (SubStep newNodeArity newNodeColor newNodeIndex) = insertNode relabeledTree currentStep (SubStep newNodeArity newNodeColor newNodeIndex)
    where 
        idxOfChange = fromIntegral $ newNodeIndex
        incrAmount = fromIntegral $ newNodeArity
        relabeledTree = updateNodeIndices currentTree idxOfChange incrAmount

applySubSteps :: IncTree -> Int -> [SubStep] -> IncTree

applySubSteps currentTree currentTick substeps = fst $ runState (visitAndLabel foldedTree) 1
    where
        foldedTree = foldl (\tree step -> insertNode tree currentTick step) currentTree substeps

unlabeledTree = Node 0 0 [Leaf 0, Node 1 0 [Leaf 0, Leaf 0], Leaf 0]
visitAndLabel :: IncTree -> State Int IncTree --relabels leaves by dfs, state int keeps latest var

visitAndLabel (Leaf idx) = do
    yo <- get
    put $ yo + 1
    return (Leaf yo)

visitAndLabel (Node idx clr children) = do
    updatedChildren <- mapM visitAndLabel children
    return (Node idx clr updatedChildren)

genClrFun :: Integer -> Integer -> Float --scales colors to go between 0 and 1 in increments of 1/maxColor

genClrFun maxColor = \x -> ((fromIntegral $ x) / (fromIntegral $ maxColor)) :: Float

treeToDot :: IncTree -> (Integer -> Float) -> (Integer -> Float) -> State Int String

treeToDot (Leaf idx) _ _ = do
    currentName <- get
    put $ currentName + 1
    currentName <- get
    return (show currentName ++ "[style=dashed]" ++ "\n")

treeToDot (Node idx clr children) nodeClrFunction edgeClrFunction = do
    currentName <- get
    put $ currentName + 1
    currentName <- get

    let (hNode,sNode,vNode) = (0.5, 0.5, (nodeClrFunction . fromIntegral $ clr))
    let (hEdge,sEdge,vEdge) = (0.7, 0.5, (edgeClrFunction . fromIntegral $ idx))

    let nodeColor = "[color=" ++ "\"" ++ (show hNode) ++ " " ++ (show sNode)  ++ " " ++  (show vNode) ++ "\"" ++  "]"
    let edgeColor = "[color=" ++ "\"" ++ (show hEdge) ++ " " ++ (show sEdge)  ++ " " ++  (show vEdge) ++ "\"" ++  "]"


    yo <- mapM (\x -> treeToDot x nodeClrFunction edgeClrFunction) children

    return $ (show currentName ++ " " ++ edgeColor ++ "\n" ++ show currentName ++ " " ++ nodeColor ++ "\n") ++ (concat $ map (\x -> (show currentName) ++ "--" ++ x) yo)

generateAndOutputTree n = do
    g <- getStdGen
    let initialTree = Leaf 1
    let steps = zip [1..] (sampleTree g n)
    let producedTree = foldl (\tree (tick, steps) -> applySubSteps tree tick steps) initialTree steps 
    print producedTree
    mapM_ print steps
    putStrLn "\n"
    let res = fst $ runState (treeToDot producedTree (genClrFun 5) (genClrFun (fromIntegral $ length steps))) 1
    putStrLn $  res
    --now to write it into a dotfile
    let output = "strict graph { \nnode [shape=point] \n" ++ res ++ "\n }"
    writeFile "generatedTree.dot" output